package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contract.Detector;
import com.twuc.webApp.data.Detectors;
import com.twuc.webApp.exception.ErrorResponse;
import com.twuc.webApp.exception.NotFoundIdException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * TODO: 测试方法中传的Detector可以提取到setUp()中
 */
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApiTestBase2 {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private TestRestTemplate testRestTemplate;


    @BeforeEach
    void setUp() {
        Detector detector1 = new Detector("rover1", 2, 4, "E");
        Detector detector2 = new Detector("rover2", 3, 4, "N");
        Detector detector3 = new Detector("rover3", 4, 5, "S");
        Detectors.detectorHashMap.put(detector1.getId(), detector1);
        Detectors.detectorHashMap.put(detector2.getId(), detector2);
        Detectors.detectorHashMap.put(detector3.getId(), detector3);
    }

    @Test
    void should_return_201_and_create_detector() throws Exception {
        Detector detector = new Detector("rover4", 2, 4, "E");
        String s = objectMapper.writeValueAsString(detector);
        mockMvc.perform(post("/api/rovers").content(s).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(201));
    }

    /**
     * 通过TestRestTemplate
     */
    @Test
    void should_return_400_if_parameter_invalid() throws Exception {
        Detector detector = new Detector("rover1", 2, 4, "G");
        ResponseEntity<ErrorResponse> responseEntity = testRestTemplate.postForEntity("/api/rovers", detector, ErrorResponse.class);
        ErrorResponse body = responseEntity.getBody();
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(body.getErrorTypeName(), IllegalArgumentException.class.getName());

    }

    /**
     * 通过MockMvc的方式来进行测试
     */
    @Test
    void should_return_400_if_parameter_invalid2() throws Exception {
        Detector detector4 = new Detector("rover4", 2, 4, "G");
        Detector detector5 = new Detector("rover5", 2, 4, "E");
        String s = objectMapper.writeValueAsString(detector4);
        String s2 = objectMapper.writeValueAsString(detector5);
        mockMvc.perform(post("/api/rovers").content(s).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(400)).andExpect(jsonPath("$.errorTypeName").value(IllegalArgumentException.class.getName()));

        mockMvc.perform(post("/api/rovers").content(s2).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(409));

    }


    @Test
    void should_return_404_if_detector_not_found() throws Exception {
        ResponseEntity<ErrorResponse> responseEntity = testRestTemplate.getForEntity("/api/rovers?id=rover6", ErrorResponse.class);
        ErrorResponse body = responseEntity.getBody();
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertEquals(body.getErrorTypeName(), NotFoundIdException.class.getName());
    }


}
