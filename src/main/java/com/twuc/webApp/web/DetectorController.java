package com.twuc.webApp.web;

import com.twuc.webApp.contract.Command;
import com.twuc.webApp.contract.Detector;
import com.twuc.webApp.service.DetectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class DetectorController {

    @Autowired
    DetectorService detectorService;


    @PostMapping("/rovers")
    public ResponseEntity createDetector(@RequestBody @Valid Detector detector, Errors errors) {

        return detectorService.createDetector(detector, errors);
    }

    @GetMapping("/rovers")
    public ResponseEntity getDetector(@RequestParam String id) {

        return detectorService.getDetectorById(id);
    }

    @PatchMapping("/rovers")
    // 测试数据：{"command" : "LLLLMRRM"}
    public ResponseEntity moveDetector(@RequestParam String id, @RequestBody @Valid Command command, Errors errors) {
        return detectorService.moveDetectorByIdAndCommand(id, command,errors);
    }
}
