package com.twuc.webApp.exception;

/**
 * @author shuang.kou
 */
public class NotFoundIdException extends RuntimeException {
    private String message;

    public NotFoundIdException() {
        super();
    }

    public NotFoundIdException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
