package com.twuc.webApp.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author shuang.kou
 */
@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {

    ErrorResponse illegalArgument = new ErrorResponse(new IllegalArgumentException());
    ErrorResponse notFountID = new ErrorResponse(new NotFoundIdException("Sorry, the detector you need is not found because the id  does not exist."));

    @ExceptionHandler(value = Exception.class)// 拦截所有异常
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception e) {

        if (e instanceof IllegalArgumentException) {
            return ResponseEntity.status(400).body(illegalArgument);
        } else if (e instanceof NotFoundIdException) {
            return ResponseEntity.status(404).body(notFountID);
        }
        return null;
    }
}
