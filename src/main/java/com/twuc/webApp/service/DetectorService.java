package com.twuc.webApp.service;

import com.twuc.webApp.contract.Command;
import com.twuc.webApp.contract.Detector;
import com.twuc.webApp.data.Detectors;
import com.twuc.webApp.exception.NotFoundIdException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@Service
public class DetectorService {

    /**
     * 1. create detector
     */
    public ResponseEntity createDetector(Detector detector, Errors errors) {
        if (errors.hasErrors()) {
            throw new IllegalArgumentException();
        }
        if (Detectors.detectorHashMap.containsKey(detector.getId())) {
            return ResponseEntity.status(409).build();
        }
        Detectors.detectorHashMap.put(detector.getId(), detector);
        return ResponseEntity.status(201)
                .header("content-type", "application/json")
                .build();
    }

    /**
     * 2. get detector by id
     */

    public ResponseEntity getDetectorById(String id) {

        if (Detectors.detectorHashMap.containsKey(id)) {
            return ResponseEntity.status(200).body(Detectors.detectorHashMap.get(id));
        } else {
            throw new NotFoundIdException();
        }
    }

    /**
     * 3. 移动 detector
     */

    public ResponseEntity moveDetectorByIdAndCommand(String id, Command command, Errors errors) {
        if (errors.hasErrors() || command == null) {
            throw new IllegalArgumentException();
        }
        if (Detectors.detectorHashMap.containsKey(id)) {
            Detector detector = moveDetector(Detectors.detectorHashMap.get(id), command.getCommand());
            Detectors.detectorHashMap.put(id, detector);
            return ResponseEntity.status(200).body(Detectors.detectorHashMap.get(id));
        }
        return ResponseEntity.status(404).build();
    }

    public Detector moveDetector(Detector detector, String command) {
        HashMap<String, Integer> directionMap = new HashMap();

        directionMap.put("N", 1);
        directionMap.put("E", 2);
        directionMap.put("S", 3);
        directionMap.put("W", 4);

        String[] chars = command.split("");

        String direction = detector.getDirection();
        int directionCode = directionMap.get(direction);

        for (int i = 0; i < chars.length; i++) {
            if ("L".equals(chars[i])) {
                directionCode -= 1;
                if (directionCode == 0) {
                    directionCode = 4;
                }
            } else if ("R".equals(chars[i])) {
                directionCode = (directionCode + 1) % 4;
            } else if ("M".equals(chars[i])) {
                moveDirection(detector, directionCode);
            }
        }
        detector.setDirection(getKey(directionMap, directionCode));
        return detector;
    }

    public void moveDirection(Detector detector, Integer directionCode) {
        if (1 == directionCode) {
            detector.setY(detector.getY() + 1);
        } else if (3 == directionCode) {
            detector.setY(detector.getY() - 1);
        } else if (2 == directionCode) {
            detector.setX(detector.getX() + 1);
        } else {
            detector.setX(detector.getX() - 1);
        }
    }

    public String getKey(HashMap hashMap, int directionCode) {

        Set set = hashMap.entrySet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            if (entry.getValue().equals(directionCode)) {
                return entry.getKey().toString();
            }
        }
        return null;
    }

}
