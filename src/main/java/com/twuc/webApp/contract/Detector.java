package com.twuc.webApp.contract;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author shuang.kou
 */
@NotNull
public class Detector {


    @Pattern(regexp = "(^[a-z0-9]*)")
    private String id;

    private Integer x;

    private Integer y;

    @Pattern(regexp = "([EWSN])")
    private String direction;

    public Detector() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public Detector(String id, Integer x, Integer y, String direction) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.direction = direction;
    }
}
