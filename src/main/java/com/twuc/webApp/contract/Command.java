package com.twuc.webApp.contract;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class Command {
    @NotNull
    @Pattern(regexp = "[LMR]*")
    String command;

    public Command() {
    }

    public Command(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }
}
